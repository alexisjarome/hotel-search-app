package hotel.search.app



import grails.test.mixin.*

import org.junit.*

import cmd.HotelResult
import cmd.HotelSearchForm

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(HotelService)
@Mock([Hotel, Country, Location, CurrencyService])
class HotelServiceTests {


		void testGetHotelsBySearchParams() {
			def countryTest = new Country(name: "United States", isoCode: "US")
			mockDomain(Country, [countryTest])
	
			assertEquals(1, Country.findAll().size())
	
			def locationTest = new Location(address: "Location 1", country: countryTest)
			def locationTest2 = new Location(address: "Location 2", country: countryTest)
			def locationInstances = [locationTest, locationTest2]
			mockDomain(Location, locationInstances)
			
			assertEquals(2, Location.findAll().size())
			assertEquals(1, Location.findAll().get(0).id)
	
			def hotelInstances = [
				new Hotel(name: "hotel 1", rate: 100, location: locationTest),
				new Hotel(name: "hotel 2", rate: 200, location: locationTest),
				new Hotel(name: "hotel 3", rate: 300, location: locationTest),
				new Hotel(name: "hotel 4", rate: 400, location: locationTest),
				new Hotel(name: "hotel 5", rate: 500, location: locationTest2)
			]
			mockDomain(Hotel, hotelInstances)
			

			assertEquals(5, Hotel.findAll().size())
			assertEquals(4, Hotel.findAllByLocation(Location.get(1)).size())
	
			def hotelService = new HotelService()
			def convertedRate = 30.5
			def currencyServiceMock = mockFor(CurrencyService)
			currencyServiceMock.demand.exchangeRate(1..1) {def currency -> return convertedRate}
			currencyServiceMock.demand.conversionRate(4..4) {def amount, def exchangeRate -> return amount * exchangeRate}
			hotelService.currencyService = currencyServiceMock.createMock()
	
			def start = new Date()
			def end = start.plus(10)
			def interval = (end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24)
	
			def hotelSearchForm = new HotelSearchForm(1, start, end, "EUR")
	
			List<HotelResult> hotelResults = hotelService.getHotelsBySearchParams(hotelSearchForm)
			assertEquals(4, hotelResults.size())
			assertEquals("hotel 1", hotelResults.get(0).hotelName)
			assertEquals("hotel 2", hotelResults.get(1).hotelName)
			assertEquals("hotel 3", hotelResults.get(2).hotelName)
			assertEquals("hotel 4", hotelResults.get(3).hotelName)
			
			assertEquals(interval * hotelResults.get(0).origRate * convertedRate, hotelResults.get(0).totalPrice)
			assertEquals(interval * hotelResults.get(1).origRate * convertedRate, hotelResults.get(1).totalPrice)
			assertEquals(interval * hotelResults.get(2).origRate * convertedRate, hotelResults.get(2).totalPrice)
			assertEquals(interval * hotelResults.get(3).origRate * convertedRate, hotelResults.get(3).totalPrice)
		}
}
