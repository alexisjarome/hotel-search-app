package cmd


class HotelResult {
	
	String hotelName
	double origRate
	double convertedRate
	double totalPrice
	int numberOfDays
	String currency
	double exchangeRate
	
	String getConvertedRateStr() {
		return String.format("%s %s", currency, convertedRate)
	}
	
	String getOrigRateStr() {
		return String.format("USD %s ", origRate)
	}
	
	String getTotalPriceStr() {
		return String.format("%s %s", currency, totalPrice)
	}
	
	String getExchangeRateStr() {
		return String.format("%s (%s)", exchangeRate, currency)
	}

	
	
	
		
	

}
