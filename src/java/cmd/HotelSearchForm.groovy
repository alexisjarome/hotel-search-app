package cmd

import org.apache.commons.logging.LogFactory
import java.util.Date;

class HotelSearchForm {

	Date startDate
	Date endDate
	long locationId
	String currency
	
	public HotelSearchForm() {
		
	}
	
	public HotelSearchForm(long locationId, Date startDate, Date endDate, String currency) {
		this.startDate = startDate
		this.endDate = endDate
		this.locationId = locationId
		this.currency = currency
	}
	
	
	long getLocationIdOtherMethod() {
		return locationId
	}
}
