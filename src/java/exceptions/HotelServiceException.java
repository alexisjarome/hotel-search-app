package exceptions;

public class HotelServiceException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HotelServiceException(String s) {
        super(s);
    }
	
	public HotelServiceException(String s, Throwable t) {
		super(s, t);
	}

}
