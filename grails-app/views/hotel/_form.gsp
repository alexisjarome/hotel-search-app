<%@ page import="hotel.search.app.Hotel" %>



<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'location', 'error')} required">
	<label for="location">
		<g:message code="hotel.location.label" default="Location" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="location" name="location.id" from="${hotel.search.app.Location.list()}" optionKey="id" required="" value="${hotelInstance?.location?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="hotel.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${hotelInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'price', 'error')} required">
	<label for="price">
		<g:message code="hotel.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="price" value="${fieldValue(bean: hotelInstance, field: 'price')}" required=""/>
</div>

