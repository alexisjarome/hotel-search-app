<%@ page import="hotel.search.app.Location" %>



<div class="fieldcontain ${hasErrors(bean: locationInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="location.address.label" default="Address" />
		
	</label>
	<g:textField name="address" value="${locationInstance?.address}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: locationInstance, field: 'country', 'error')} required">
	<label for="country">
		<g:message code="location.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="country" name="country.id" from="${hotel.search.app.Country.list()}" optionKey="id" optionValue="code" required="" value="${locationInstance?.country?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: locationInstance, field: 'hotels', 'error')} ">
	<label for="hotels">
		<g:message code="location.hotels.label" default="Hotels" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${locationInstance?.hotels?}" var="h">
    <li><g:link controller="hotel" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="hotel" action="create" params="['location.id': locationInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'hotel.label', default: 'Hotel')])}</g:link>
</li>
</ul>

</div>

