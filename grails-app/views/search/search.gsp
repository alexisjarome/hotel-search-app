<%@ page import="cmd.HotelResult" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="hotel.search.title.label"  /></title>
	</head>
	<body>
		<a href="#list-hotel-results" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><a class="admin" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></li>
			</ul>
		</div>
		
		<div id="list-country" class="content scaffold-list" role="main">
			<h1>Hotel Search</h1>
			<g:form method="post" action="performSearch">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton class="search" name="search" value="${message(code: 'hotel.search.button.label', default: 'Search')}" />
				</fieldset>
			</g:form>
			
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="hotelName" title="${message(code: 'search.name.label', default: 'Name')}" />					
						<th>${message(code: 'search.numberOfDays.label', default: 'Number of Days')}</th>
						<g:sortableColumn property="origRate" title="${message(code: 'search.origRate.label', default: 'Original Rate')}" />
						<th>${message(code: 'search.exchangeRate.label', default: 'Excahnge Rate')}</th>
						<g:sortableColumn property="convertedRate" title="${message(code: 'search.convertedRate.label', default: 'Converted Rate')}" />
						<g:sortableColumn property="totalPrice" title="${message(code: 'search.totalPrice.label', default: 'Total Price')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${hotelResults}" status="i" var="hotelResult">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${fieldValue(bean: hotelResult, field: "hotelName")}</td>
						<td>${fieldValue(bean: hotelResult, field: "numberOfDays")}</td>
						<td>${fieldValue(bean: hotelResult, field: "origRateStr")}</td>
						<td>${fieldValue(bean: hotelResult, field: "exchangeRateStr")}</td>
						<td>${fieldValue(bean: hotelResult, field: "convertedRateStr")}</td>
						<td>${fieldValue(bean: hotelResult, field: "totalPriceStr")}</td>				
					</tr>
				</g:each>
				</tbody>
			</table>
			<g:if test="${hotelResultsTotal}">
				<div class="pagination">
					<g:paginate total="0" />
				</div>
			</g:if>
		</div>
	</body>
</html>
