

	<div>
		<label for="startDate">
			<g:message code="hotel.search.startDate.label" default="Check In Date" />
		</label>
		<g:datePicker name="startDate" precision="day" value="${hotelSearchForm?.startDate}" default="${new Date()}"/>
	</div>
	<div>
		<label for="endDate">
			<g:message code="hotel.search.endDate.label" default="Check Out Date" />
		</label>
		<g:datePicker name="endDate" precision="day" value="${hotelSearchForm?.endDate}" default="${new Date().plus(1)}"/>
	</div>
	<div>
		<label for="wow">
			<g:message code="hotel.search.location.label" default="Location" />
		</label>
		<g:select id="locationId" name="locationId" from="${hotel.search.app.Location.list()}" optionKey="id" required="" value="${hotelSearchForm?.locationId}" class="many-to-one"/>
	</div>
	<div>
		<label for="currency">
			<g:message code="hotel.search.currency.label" default="Currency" />
		</label>
		<g:select id="currency" name="currency" optionKey="code" from="${currencyList}" value="${hotelSearchForm?.currency}"  required="" class="many-to-one"/>
	</div>
