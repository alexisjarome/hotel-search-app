<%@ page import="hotel.search.app.Country" %>
<%@ page import="org.grails.plugins.exchangerates.ExchangeCurrency" %>



<div class="fieldcontain ${hasErrors(bean: countryInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="country.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${countryInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: countryInstance, field: 'isoCode', 'error')} required">
	<label for="isoCode">
		<g:message code="country.isoCode.label" default="Iso Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="isoCode" maxlength="2" required="" value="${countryInstance?.isoCode}"/>
</div>
