<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="admin.title.label" /></title>
	</head>
	<body>
		<a href="#show-country" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><a class="admin" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></li>
			</ul>
		</div>
		
		<div id="list-domains" class="content" role="main">
				<h2>Available Domains:</h2>
				<ol class="property-list">
					<li><a class="hotels" href="${createLink(uri: '/admin/hotel')}"><g:message code="default.hotel.label"/></a></li>
					<li><a class="locations" href="${createLink(uri: '/admin/location')}"><g:message code="default.location.label"/></a></li>
					<li><a class="countries" href="${createLink(uri: '/admin/country')}"><g:message code="default.country.label"/></a></li>
				</ol>
		</div>
	</body>
</html>
