package hotel.search.app

class Location {
	String address
	Country country
	
	static hasMany = [hotels : Hotel]

	String toString() {
		return new StringBuilder(address).append(", ").append(country.getName()).toString()
	}
	
    static constraints = {
		address blank : false, uniqe: true
		country nullable : false
    }
}
