package hotel.search.app

class Hotel {

	String name
	double rate

	static belongsTo = [location : Location]
	
    static constraints = {
		name blank : false, unique: true		
    }
	
	String getRateStr() {
		return String.format("USD %s", rate)
	}
}
