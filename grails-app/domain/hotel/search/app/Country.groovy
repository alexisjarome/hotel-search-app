package hotel.search.app

class Country {	
	String name
	String isoCode
	
	String toString() {
		return isoCode
	}
    static constraints = {
		name  blank: false, unique: true
		isoCode blank: false, size: 2..2
    }
}
