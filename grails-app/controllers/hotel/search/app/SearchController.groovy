package hotel.search.app

import cmd.HotelResult
import cmd.HotelSearchForm
import exceptions.HotelServiceException

class SearchController {
	
	def hotelService
	def currencyService

	def main() {
		def currencies = currencyService.getCurrencies()
		def hotelSearchForm = new HotelSearchForm()
		
		render(view: "search", model: [currencyList : currencies, hotelSearchForm : hotelSearchForm])
	}
	
	def performSearch() {		
		long locationId = Long.valueOf(params["locationId"])
		def start = params["startDate"]
		def end = params["endDate"]
		def currency = params["currency"]

		def hotelSearchForm = new HotelSearchForm(locationId, start, end, currency)
		List<HotelResult> hotelResults = null
		flash.message = null
		try {
			hotelResults = hotelService.getHotelsBySearchParams(hotelSearchForm)
			if (hotelResults.size == 0) {
				flash.message = message(code: 'hotel.search.no.results')
			}
		} catch (e) {
			if (!e.getCause()) {
				flash.message = message(code: 'hotel.search.error.api')
			} else if (e.getCause() instanceof HotelServiceException) {
				flash.message = message(code: 'hotel.search.error.exchangeRate', args: [currency])
			}
			
			hotelResults = Collections.emptyList()
		}
		
	
		def currencies = currencyService.getCurrencies()
		render(view: "search", model: [currencyList : currencies, hotelResults: hotelResults, hotelSearchForm : hotelSearchForm, hotelResultsTotal : hotelResults.size()])
	}
	
	def handleHotelServiceException(HotelServiceException e) {
		log.info("E!!!!: " + e.message)
	}
}
