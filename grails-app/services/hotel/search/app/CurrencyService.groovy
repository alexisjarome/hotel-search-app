package hotel.search.app

import java.math.RoundingMode;

import org.grails.plugins.exchangerates.CurrencyInitialData
import org.springframework.transaction.annotation.Transactional

import cmd.Currency


@Transactional
class CurrencyService {
    static String US_DOLLAR_CODE = "USD"
	def exchangeRateService
	
	def getCurrencies() {
		
		def currencies = new CurrencyInitialData().getCurrencies()
		def currenciesData = new ArrayList<Currency>()

		for (i in currencies) {
			def currency = new Currency()
			currency.code = i["code"]
			currency.name = i["name"]
			currenciesData.add(currency)
		}
		
		return currenciesData

	}
	
	def conversionRate(BigDecimal amount, BigDecimal exchangeRate) {
		def rawValue = amount * exchangeRate
		return rawValue
	}
	
	def exchangeRate(String exchangeTo) {
		def rate = exchangeRateService.dynamicRate("USD", exchangeTo)
		log.info("exchangeRate() rate: " + rate)
		return rate
	}
	

}
