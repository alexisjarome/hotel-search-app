package hotel.search.app




import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Transactional

import cmd.HotelResult
import cmd.HotelSearchForm
import exceptions.HotelServiceException

class HotelService {
	def currencyService
	
    def serviceMethod() {

    }
	
	List<HotelResult> getHotelsBySearchParams(HotelSearchForm form) {
		long locationId = form.locationId
		def start = form.getStartDate()
		def end = form.getEndDate()
		def currency = form.getCurrency()
		
		Location location = Location.get(locationId)
		
		
		List<Hotel> hotels =  location ? Hotel.findAllByLocation(location) : Collections.emptyList()
		List<HotelResult> hotelResults = new ArrayList<HotelResult>()
		
		def interval = (end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24)
		def exchangeRate = null
		try {
			exchangeRate = currencyService.exchangeRate(currency)
		} catch(Exception e) {
			log.error("Api Error!")
			throw new HotelServiceException("Api Error!")
		}
		
		if (!exchangeRate) {
			log.error("No available exchange rate for currency!")
			throw new HotelServiceException("No available exchange rate for currency!")
		}
		
		for (Hotel hotel : hotels) {
			BigDecimal totalAmount = interval * hotel.rate
			def totalConvertedAmount = currencyService.conversionRate(totalAmount, exchangeRate)
			
			def hotelResult = new HotelResult()
			hotelResult.hotelName = hotel.name
			hotelResult.numberOfDays = interval
			hotelResult.origRate = hotel.rate
			hotelResult.currency = currency
			hotelResult.exchangeRate = exchangeRate
			hotelResult.convertedRate = exchangeRate * hotel.rate
			hotelResult.totalPrice = totalConvertedAmount

			
			hotelResults.add(hotelResult)
		}

		return hotelResults
	}
}
