import hotel.search.app.Country
import hotel.search.app.Hotel;
import hotel.search.app.Location


class BootStrap {

    def init = { servletContext ->
		
		if (!Country.count()) {
			new Country(name : "United States", isoCode : "US").save()
			new Country(name : "Philippines", isoCode : "PH").save()
			new Country(name : "Japan", isoCode : "JP").save()
			new Country(name : "Australia", isoCode : "AU").save()
			new Country(name : "Spain", isoCode : "ES").save()
			new Country(name : "Germany", isoCode : "DE").save()
			new Country(name : "Brazil", isoCode : "BR").save()
			new Country(name : "Portugal", isoCode : "PT").save()
		}
		
		
		if (!Location.count()) {
			new Location(address : "Test Address 1", country : Country.findByName("United States")).save()
			new Location(address : "Test Address 2", country : Country.findByName("United States")).save()
			new Location(address : "Test Address 3", country : Country.findByName("United States")).save()
			new Location(address : "Test Address 4", country : Country.findByName("United States")).save()
			new Location(address : "Test Address 5", country : Country.findByName("United States")).save()
			
			if (!Hotel.count()) {
				new Hotel(name : "Test Hotel 1.0", rate : 120, location : Location.findByAddress("Test Address 1")).save()
				new Hotel(name : "Test Hotel 1.1", rate : 150, location : Location.findByAddress("Test Address 1")).save()
				new Hotel(name : "Test Hotel 1.2", rate : 200, location : Location.findByAddress("Test Address 1")).save()
				new Hotel(name : "Test Hotel 1.3", rate : 250, location : Location.findByAddress("Test Address 1")).save()
				new Hotel(name : "Test Hotel 2.0", rate : 100, location : Location.findByAddress("Test Address 2")).save()
				new Hotel(name : "Test Hotel 2.1", rate : 150, location : Location.findByAddress("Test Address 2")).save()
				new Hotel(name : "Test Hotel 2.2", rate : 300, location : Location.findByAddress("Test Address 2")).save()
				new Hotel(name : "Test Hotel 2.3", rate : 400, location : Location.findByAddress("Test Address 2")).save()
				new Hotel(name : "Test Hotel 3.0", rate : 250, location : Location.findByAddress("Test Address 3")).save()
				new Hotel(name : "Test Hotel 3.1", rate : 350, location : Location.findByAddress("Test Address 3")).save()
				new Hotel(name : "Test Hotel 4.0", rate : 500, location : Location.findByAddress("Test Address 4")).save()
			}
		}
    }
    def destroy = {
    }
}
