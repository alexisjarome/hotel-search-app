class UrlMappings {

	static mappings = {
		"/admin/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller:"search", action:"main")			
		"/admin"(controller:"admin", action:"goTo")
//		"500"(view:'/error')
	}
}
